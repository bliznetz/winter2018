BEGIN {
	MIN = 0;
	MAX = 101;
	INTERVAL_SIZE = 10;
}
{
	DATA = $1;
	if (DATA !~ "^[0-9]+(.[0-9]+)?$") {
		printf("ERROR at %s:%i: First column must be numbers. \"%s\" is not a number.\n", FILENAME, FCR, DATA);
		next;
	}
	if (DATA < MIN || DATA > MAX) {
		printf("Error: Value %i is out of range %i - %i\n", $1, MIN, MAX);
		next;
	}

	ARRAY[ int ((DATA - MIN) / INTERVAL_SIZE) ]++;
}
END {
	INDEX = 0;
	for (i = MIN; i < MAX; i += INTERVAL_SIZE){
		printf("%i - %i: ", i, i + INTERVAL_SIZE - 1);
		for (j = 0; j < ARRAY[INDEX]; ++j) {
			printf("*");
		}
		printf("\n");
		++INDEX;
	}
}