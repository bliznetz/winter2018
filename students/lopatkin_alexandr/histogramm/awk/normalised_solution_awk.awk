BEGIN {
	MIN = 0;
	MAX = 101;
	INTERVAL_SIZE = 10;
	VALID_RECORD_COUNTER=0;
}
{
	DATA = $1;
	if (DATA !~ "^[0-9]+(.[0-9]+)?$") {
		printf("ERROR at %s:%i: First column must be numbers. \"%s\" is not a number.\n", FILENAME, FCR, DATA);
		next;
	}
	if (DATA < MIN || DATA > MAX) {
		printf("Error: Value %i is out of range %i - %i\n", $1, MIN, MAX);
		next;
	}

	ARRAY[ int ((DATA - MIN) / INTERVAL_SIZE) ]++;
	VALID_RECORD_COUNTER++;
}
END {
	if (VALID_RECORD_COUNTER == 0) {
		printf("There are no valid records in file:%s. Exit program with code 1.\n", FILENAME);
		exit 1;
	}
	INDEX = 0;
	for (i = MIN; i < MAX; i += INTERVAL_SIZE){
		TEMP += ARRAY[INDEX] / (VALID_RECORD_COUNTER);
		printf("%i - %i: %f%\n", i, i + INTERVAL_SIZE - 1, ARRAY[INDEX] / (VALID_RECORD_COUNTER) * 100);
		++INDEX;
	}
}