BEGIN{
}
{if ($1<10) 
	arr["0-9"]+=1
else if ($1<20)
	arr["10-19"]+=1
else if ($1<30)
	arr["20-29"]+=1
else if ($1<40)
	arr["30-39"]+=1
else if ($1<50)
	arr["40-49"]+=1
else if ($1<60)
	arr["50-59"]+=1
else if ($1<70)
	arr["60-69"]+=1
else if ($1<80)
	arr["70-79"]+=1
else if ($1<90)
	arr["80-89"]+=1
else if ($1<100)
	arr["90-99"]+=1
else
	arr["100"]+=1
}
END {
for (x in arr) {
	arr[x] = arr[x]/2
}

pattern="%-9s%6.1f%% %s\n"
ast=""
for (i=0;i<arr["0-9"];i++)
ast=sprintf("%s%s",ast,"*")
printf(pattern, "  0 -  9:", arr["0-9"], ast)
ast=""
for (i=0;i<arr["10-19"];i++)
ast=sprintf("%s%s",ast,"*")
printf(pattern, " 10 - 19:", arr["10-19"], ast)
ast=""
for (i=0;i<arr["20-29"];i++)
ast=sprintf("%s%s",ast,"*")
printf(pattern, " 20 - 29:", arr["20-29"], ast)
ast=""
for (i=0;i<arr["30-39"];i++)
ast=sprintf("%s%s",ast,"*")
printf(pattern, " 30 - 39:", arr["30-39"], ast)
ast=""
for (i=0;i<arr["40-49"];i++)
ast=sprintf("%s%s",ast,"*")
printf(pattern, " 40 - 49:", arr["40-49"], ast)
ast=""
for (i=0;i<arr["50-59"];i++)
ast=sprintf("%s%s",ast,"*")
printf(pattern, " 50 - 59:", arr["50-59"], ast)
ast=""
for (i=0;i<arr["60-69"];i++)
ast=sprintf("%s%s",ast,"*")
printf(pattern, " 60 - 69:", arr["60-69"], ast)
ast=""
for (i=0;i<arr["70-79"];i++)
ast=sprintf("%s%s",ast,"*")
printf(pattern, " 70 - 79:", arr["70-79"], ast)
ast=""
for (i=0;i<arr["80-89"];i++)
ast=sprintf("%s%s",ast,"*")
printf(pattern, " 80 - 89:", arr["80-89"], ast)
ast=""
for (i=0;i<arr["90-99"];i++)
ast=sprintf("%s%s",ast,"*")
printf(pattern, " 90 - 99:", arr["90-99"], ast)
ast=""
for (i=0;i<arr["100"];i++)
ast=sprintf("%s%s",ast,"*")
printf(pattern, "100:", arr["100"], ast)
}

