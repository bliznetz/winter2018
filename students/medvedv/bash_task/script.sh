#!/usr/bin/env bash

echo "number of parameters: "$#

echo "program name: "$0

ln -s $0 link_to_script.sh

shift 1

echo "first 3 parameters are: " $1 $2 $3

#
#./link_to_script.sh I am user $USER
#./link_to_script.sh "I am $USER"
#./link_to_script.sh 'I am $USER'