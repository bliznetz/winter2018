#!/usr/bin/env bash


SHORTOPT="f:l:a"
LONGOPT="file:,log:,append"

OPTS=$(getopt -o "$SHORTOPT" -l "$LONGOPT" -- "$@")

eval set -- $OPTS


while [ $# -gt 0 ]; do
    case $1 in
        -f | --file ) FILENAME=$2; shift 2;;
        -l | --log ) LOG=$2; if [ x$LOG == x ]; then echo "Enter string to log" &>2; exit 2; fi; shift 2;;
        -a | --append ) APPEND=1; shift;;
        --) shift; break;;
        *) shift; ;;
    esac
done

if [ x$FILENAME == x ]; then echo "Enter filename" &>2; exit 1; fi;


if [ x$APPEND == x1 ]; then
    echo `date` $LOG >> $FILENAME;
else
    echo `date` $LOG > $FILENAME;
fi