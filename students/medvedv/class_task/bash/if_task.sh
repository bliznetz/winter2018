#!/usr/bin/env bash

FILENAME=$1

if [ -e $FILENAME ]; then
    echo "file $1 exists"
    if [[ $(file $FILENAME) =~ "script" ]]; then
        ./$FILENAME
    fi
    
    if [ -d $FILENAME ]; then
        ls -lS $FILENAME | head -n 6
    fi    
else
    mkdir $1
fi
