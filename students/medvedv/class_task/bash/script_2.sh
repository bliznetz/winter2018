#!/usr/bin/env bash

#sum
SUM=0
for i in {1..100}; do
    : $((SUM += i**2)) 
done
echo $SUM

#random to file
for i in {1..10}; do
    echo $((RANDOM % 80)) >> random.txt
done


#histogram
exec 10<>random.txt
array=()
while read -u 10 number ; do
    : $((array[number] += 1))
done
for i in {1..80}; do
    if [ "${array[i]}" != "" ]; then
        echo -n "$i:"
        for j in `seq ${array[i]}`; do
            echo -n "*"
        done
        echo ""
    fi
done