#!/bin/bash

#run this script as root

#public directory for all users
semodule -i selinux_policy/manager_policy.pp

mkdir /home/public
chmod 777 /home/public
cd /home/public


#creating directories and files for Projects
mkdir Proj1 Proj2 Proj3

#some sample files
touch Proj1/file1 Proj1/file2 Proj2/file.txt Proj2/file2.txt Proj3/file.txt


#groups
groupadd proj_1_rw
groupadd proj_2_rw
groupadd proj_3_rw
groupadd proj_1_r
groupadd proj_2_r
groupadd proj_3_r
groupadd managers

chgrp -R proj_1_rw Proj1
chgrp -R proj_2_rw Proj2
chgrp -R proj_3_rw Proj3

chmod -R u=rwX,g=rwXs,o=--- Proj1 Proj2 Proj3

#developers
useradd R1 -G proj_2_rw,proj_3_rw
useradd R2 -G proj_3_rw,proj_1_rw
useradd R3 -G proj_1_rw
useradd R4 -G proj_3_rw
useradd R5 -G proj_1_rw

#analytics
useradd A1 -G proj_2_rw,proj_1_rw,proj_3_r
useradd A2 -G proj_3_rw,proj_2_r
useradd A3 -G proj_2_r,proj_2_r
useradd A4 -G proj_1_r,proj_3_r

#managers
useradd I1 -G managers -Z guest_u
useradd I2 -G managers -Z guest_u
useradd I3 -G managers -Z guest_u


chcon -R -t project_file_t Proj1
chcon -R -t project_file_t Proj2
chcon -R -t project_file_t Proj3
#acl
setfacl -R -dm g:proj_1_r:r-X,g:managers:rwX,m:rwX Proj1
setfacl -R -m g:proj_1_r:r-X,g:managers:rwX,m:rwX Proj1
setfacl -R -dm g:proj_2_r:r-X,g:managers:rwX,m:rwX Proj2
setfacl -R -m g:proj_2_r:r-X,g:managers:rwX,m:rwX Proj2
setfacl -R -dm g:proj_3_r:r-X,g:managers:rwX,m:rwX Proj3
setfacl -R -m g:proj_3_r:r-X,g:managers:rwX,m:rwX Proj3
