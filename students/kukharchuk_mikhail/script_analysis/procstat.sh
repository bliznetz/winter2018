#!/bin/sh
#
#

#
# Check lock file (wait and exit)
#

LockFile=/var/run/`basename $0`.lock
Count=5
while [ -f $LockFile -a $Count -gt 0 ] ; do 		#Check file exist and it is a regular file
	sleep 3
	Count=`expr $Count - 1`
done
if [ $Count -eq 0 ] ; then			#If not, exit with error 2
	echo CRITICAL - $0 locked
	exit 2
fi

#
# Check another running procstat (wait and kill it)
#

PidFile=/var/run/`basename $0`.pid
Count=5
while [ -f $PidFile -a $Count -gt 0 ] ; do
	sleep 3
	Count=`expr $Count - 1`
done
if [ $Count -eq 0 ] ; then
	kill -9 `cat $PidFile`
	rm -f $PidFile
	echo CRITICAL - $0 already executed, killing it
	exit 2
fi
echo $$ >$PidFile

#
#
#

Verbose=0
if [ "X$1" = "X--verbose" ] ; then
	Verbose=1
fi

#
#
# in original script was /usr/local/bin/awk
/bin/ps -axw | /usr/bin/gawk -v Verbose=$Verbose '
BEGIN {
	ConfigFileName="/usr/local/etc/procstat.conf";
	LogFileName="/var/log/procstat.log";
	FixFileName="/tmp/procstat.fix";

	NProcess=0;
	while(getline <ConfigFileName) {
		if(($0 == "") || ($0 ~ /^#.*$/)) continue;
		if(tolower($1) == "process") {
			NProcess++;
			String=$2;
			for(i=3; i <=NF; i++) String=String " " $i;
			ProcessName[NProcess]=String;
			ProcessMin[NProcess]=1;
			ProcessMax[NProcess]=1;
			ProcessFix[NProcess]="_default_";
			ProcessNum[NProcess]=0;
			ProcessPid[NProcess]="";
		} else if(tolower($1) == "max") {
			ProcessMax[NProcess]=$2;
		} else if(tolower($1) == "min") {
			ProcessMin[NProcess]=$2;
		} else if(tolower($1) == "fix") {
			String=$2;
			for(i=3; i <=NF; i++) String=String " " $i;
			ProcessFix[NProcess]=String;
		} else {
			print "Illegal directive: " $0 > "/dev/stderr";
		}
	}
}
{
	ProcName=$5;
	for(i=6; i <=NF; i++) ProcName=ProcName " " $i;
	for(i=1; i <= NProcess; i++) {
		s1=ProcessName[i];
		s2=ProcName;
		if(ProcessName[i] ~ /.*\*$/) {
			l1=length(ProcessName[i]);
			s1=substr(ProcessName[i], 1, l1-1);
			l2=length(ProcName);
			if(l2 > l1)
				s2=substr(ProcName, 1, l1-1);
		}
		if(s1 == s2) {
			ProcessNum[i]++;
			ProcessPid[i]=ProcessPid[i] " " $1;
		}
	}
}
END {
	NeedFix=0;
	for(i=1; i <= NProcess; i++) {
		if((ProcessNum[i] < ProcessMin[i]) || (ProcessNum[i] > ProcessMax[i])) {
			if(NeedFix == 0)
				print "#!/bin/sh" >FixFileName;
			NeedFix++;
			if(ProcessFix[i] == "_default_") {
				if(ProcessPid[i] != "")
					print "kill -9 " ProcessPid[i] " ; " >>FixFileName;
				print ProcessName[i] >>FixFileName;
			} else {
				print ProcessFix[i] >>FixFileName;
			}
			print strftime("%Y-%m-%d %H:%M:%S"), "ErrProcNum =", ProcessNum[i], ProcessName[i] >>LogFileName;
			if(Verbose) printf("%-8s %5d %s\n", "CRITICAL", ProcessNum[i], ProcessName[i]);
		} else {
			if(Verbose) printf("%-8s %5d %s\n", "OK", ProcessNum[i], ProcessName[i]);
		}
	}
	if(NeedFix > 0)
		system("/bin/sh " FixFileName);
}' -

rm -f $PidFile
exit 0
